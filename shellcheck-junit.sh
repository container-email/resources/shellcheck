#!/bin/sh

#find files with shebang
FILES=$(grep --exclude-dir=.git --exclude-dir=*cache* -rm1 ^ ./* | grep '\:\#\!\/bin\/' | grep 'sh\|bash\|ksh' | cut -d':' -f1 | xargs)
if [ -z "$FILES" ]; then
  echo "No files to check"
  exit
fi
echo "Found shell files:"
echo "$FILES"
#create report
eval shellcheck "$FILES" | shellcheck-tty-junit

#create diff
eval shellcheck -f diff "$FILES" > shellcheck.patch

#create normal output
eval shellcheck "$FILES"