# shellcheck

Container for running shellcheck and converting output to junit results with python.

Creates:
- shellcheck.patch (shellcheck output as a diff).
- shellcheck-report.xml (test results in junit format for gitlab tests tab).

These need saving as artefacts, or use with: https://gitlab.com/container-email/resources/gitlab-templates/-/blob/main/shellcheck.yml
