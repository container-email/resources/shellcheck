ARG DVER=latest
FROM docker.io/alpine:$DVER

ENV SHELLVER=0.8.0

WORKDIR /usr/local/bin
COPY shellcheck-junit.sh shellcheck-tty-junit ./

RUN wget -O /tmp/shellcheck.tar.xz https://github.com/koalaman/shellcheck/releases/download/v${SHELLVER}/shellcheck-v${SHELLVER}.linux.$(uname -m).tar.xz \
&& apk add --no-cache -u --virtual .build-deps xz \
&& tar -xJf /tmp/shellcheck.tar.xz -C /tmp \
&& mv /tmp/shellcheck-*/shellcheck /usr/local/bin/ \
&& rm -rf /tmp/* \
&& apk del .build-deps \
&& apk add --no-cache -u grep findutils python3
